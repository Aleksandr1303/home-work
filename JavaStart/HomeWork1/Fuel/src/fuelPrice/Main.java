package fuelPrice;

public class Main {

	public static void main(String[] args) {
		// Стоимость топлива для поездки:

		double literOfFuelPrice = 1.2;
		double distance = 120;
		double fuelCons = 8;
		int interval = 100;

		double fuelPrice = distance * fuelCons / interval * literOfFuelPrice;
		System.out.println("Стоимость топлива для поездки составляет - " + fuelPrice + "$");
	}

}
