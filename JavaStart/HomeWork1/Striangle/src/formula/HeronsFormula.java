package formula;

public class HeronsFormula {

	public static void main(String[] args) {
		// Heron's formula:
		double sideA = 0.3;
		double sideB = 0.4;
		double sideC = 0.5;
		double p = 0.5*(sideA + sideB + sideC);
		double sTriangle;
		sTriangle = Math.sqrt(p*(p-sideA)*(p-sideB)*(p-sideC));
		System.out.println("площадь треугольника по формуле Герона: sTriangle = " + sTriangle);
	}

}
