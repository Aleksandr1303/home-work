package apartNumber;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// расположение квартиры:

		int floorsHouse = 9;
		int hallwaysHouse = 5;
		int apartmentsFloor = 4;
		int apartHallways;
		int numberHallways;
		int apartNumber;
		int floor;
		Scanner scan = new Scanner(System.in);
		System.out.println("Введите номер квартиры: ");
		apartNumber = scan.nextInt();

		if (apartNumber > 0 && apartNumber <= floorsHouse * hallwaysHouse * apartmentsFloor) {
			apartHallways = floorsHouse * apartmentsFloor;
			numberHallways = apartNumber / apartHallways;
			floor = (apartNumber - apartHallways * numberHallways) / apartmentsFloor;

			if (!(apartNumber % apartHallways == 0)) {
				++numberHallways;
			}

			if (!(apartNumber % apartmentsFloor == 0)) {
				++floor;
			}

			if (floor == 0) {
				floor = floorsHouse;
			}
			System.out.println("Квартира расположена в " + numberHallways + " парадной на " + floor + " этаже;");
		} else {
			System.out.println("ERROR: такой квартиры не существует!");
		}
	}

}
