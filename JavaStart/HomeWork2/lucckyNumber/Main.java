package lucckyNumber;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// Счастливое число:
		Scanner sc = new Scanner(System.in);
		System.out.println("Введите целое четырехзначное число: ");
		int number = sc.nextInt();
		System.out.println(number);

		int thousand = number / 1000;
		int hundred = (number - thousand * 1000) / 100;
		int dozens = (number - thousand * 1000 - hundred * 100) / 10;
		int units = number - thousand * 1000 - hundred * 100 - dozens * 10;

		if (thousand + hundred == dozens + units) {
			System.out.println("Поздравляем! Ваш билет счастливый!");
		} else {
			System.out.println("Увы! Ваш билет несчастливый!");
		}
	}

}
