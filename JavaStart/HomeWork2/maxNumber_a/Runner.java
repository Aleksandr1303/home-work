package maxNumber_a;

import java.util.Scanner;

public class Runner {

	public static void main(String[] args) {
		// максимальное число:
		Scanner sc = new Scanner(System.in);
		System.out.println("Введите первое число: ");
		int firstNumber = sc.nextInt();
		System.out.println("Введите второе число: ");
		int secondNumber = sc.nextInt();
		System.out.println("Введите третье число: ");
		int thirdNumber = sc.nextInt();
		System.out.println("Введите четвертое число: ");
		int fourthNumber = sc.nextInt();
		int maxNumber;
		
		maxNumber = firstNumber;
		if (secondNumber > maxNumber) {maxNumber = secondNumber;}
		if (thirdNumber > maxNumber) {maxNumber = thirdNumber;}
		if (fourthNumber > maxNumber) {maxNumber = fourthNumber;}
		System.out.println("Максимальное число: " + maxNumber);
	}

}
