package maxNumber_b;

import java.util.Scanner;

public class Runner {

	public static void main(String[] args) {
		// максимальное число:
		Scanner scan = new Scanner(System.in);
		System.out.println("Введите первое число: ");
		int firstNumber = scan.nextInt();
		System.out.println("Введите второе число: ");
		int secondNumber = scan.nextInt();
		System.out.println("Введите третье число: ");
		int thirdNumber = scan.nextInt();
		System.out.println("Введите четвертое число: ");
		int fourthNumber = scan.nextInt();
		int maxNumber;
		
		maxNumber = (firstNumber > secondNumber)? firstNumber : secondNumber;
		maxNumber = (thirdNumber > maxNumber && thirdNumber > fourthNumber)? thirdNumber : (fourthNumber > maxNumber)? fourthNumber : maxNumber;
		System.out.println("Максимальное число: " + maxNumber);
	}

}
