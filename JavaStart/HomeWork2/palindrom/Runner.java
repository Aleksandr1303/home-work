package palindrom;

import java.util.Scanner;

public class Runner {

	public static void main(String[] args) {
		// является ли число палиндромом?:

		Scanner scan = new Scanner(System.in);
		System.out.println("Введите целое шестизначное число: ");
		int number = scan.nextInt();

		int hundredsOfThousands = number / 100000;
		int tensOfThousands = (number - hundredsOfThousands * 100000) / 10000;
		int thousands = (number - hundredsOfThousands * 100000 - tensOfThousands * 10000) / 1000;
		int hundreds = (number - hundredsOfThousands * 100000 - tensOfThousands * 10000 - thousands * 1000) / 100;
		int tens = (number - hundredsOfThousands * 100000 - tensOfThousands * 10000 - thousands * 1000 - hundreds * 100)
				/ 10;
		int units = number - hundredsOfThousands * 100000 - tensOfThousands * 10000 - thousands * 1000 - hundreds * 100
				- tens * 10;

		if (hundredsOfThousands == units && tensOfThousands == tens && thousands == hundreds) {
			System.out.println("Ваше число - палиндром!");
		} else {
			System.out.println("Это обычное число.");
		}
	}

}
