package factorial;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// Вывчислить с помощью цикла факториал числа:

		Scanner sc = new Scanner(System.in);
		int n;
		System.out.println("Введите число больше 4 и меньше 16, факториал которого надо вычислить: ");
		n = sc.nextInt();
		long p = 1;

		if (n > 4 && n < 16) {
			for (int i = n; i >= 1; i--) {
				p *= i;
			}
			System.out.println("Факториал числа " + n + "! = " + p);
		} else {
			System.out.println("Вы ввели число не из заданного интервала условия!");
		}
		// Второй вариант: не надежный :)

//		for (; n >= 1; n--) {
//			p *= n;
//		}
		// или так:
//		for (int i = 1; i <= n; i++) {
//			p *= i;
//		}
	}

}
