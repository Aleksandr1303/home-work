package figure;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// Нарисовать фигуру из *:

		Scanner sc = new Scanner(System.in);
		int h;
		System.out.println("Максимальная высота фигуры: ");
		h = sc.nextInt();
		int n = 1;

		for (int i = 1; h >= 1;) {
			if (i <= n && n <= h) {
				System.out.print("*");
				i++;
			} else if(i > n){
				System.out.println();
				n++;
				i = 1;
			}
			if (i <= h && n > h) {
				System.out.print("*");
				i++;
			} else if (i > h) {
				System.out.println();
				h--;
				i = 1;
			}

		}
	}

}
