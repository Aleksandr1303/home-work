package multiplicationTable;

import java.util.Scanner;

public class Runner {

	public static void main(String[] args) {
		// таблица умножения:

		Scanner scan = new Scanner(System.in);
		int n;
		System.out.println("Таблица умножения на ?:");
		n = scan.nextInt();
		int mult = 1;

		for (int i = 1; i <= n; i++) {
			for (int j = 1; j <= n; j++) {
				mult = i * j;
				System.out.print(j + "*" + i + "=" + mult + "; ");
			}
			System.out.println();
		}

	}

}
