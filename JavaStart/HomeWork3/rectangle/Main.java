package rectangle;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// прямоугольник из *:

		Scanner sc = new Scanner(System.in);
		int height;
		int width;
		System.out.println("Введите высоту прямоугольника (числовое значение): ");
		height = sc.nextInt();
		System.out.println("Введите ширину прямоугольника (числовое значение): ");
		width = sc.nextInt();

		for (int i = 1; i <= height; i++) {
			for (int j = 1; j <= width; j++) {
				if (i == height || i == 1 || j == 1 || j == width) {
					System.out.print("*");
				} else {
					System.out.print(" ");
				}

			}
			System.out.println();
		}

		// или так:

//		for (int i = 1; i <= height; i++) {
//			for (int j = 1; j <= width; j++) {
//				if ((i > 1 && i < height) && (j > 1 && j < width)) {
//					System.out.print(" ");
//				} else {
//					System.out.print("*");
//				}
//
//			}
//			System.out.println();
//		}
	}

}
