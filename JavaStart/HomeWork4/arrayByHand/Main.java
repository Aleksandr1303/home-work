package arrayByHand;

import java.util.Arrays;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// Массив целых чисел, заполненный вручную:

		int[] arr;
		int amount;

		Scanner sc = new Scanner(System.in);
		System.out.println("Введите количество элементов в массиве:");
		amount = sc.nextInt();

		arr = new int[amount];

		for (int i = 0, j = 1; i < arr.length; i++) {
			System.out.println("Введите " + j + "-ое значение элемента массива:");
			arr[i] = sc.nextInt();
			j++;
		}
		System.out.println("Ваш массив выглядит так: " + Arrays.toString(arr) + ".");
	}
}
