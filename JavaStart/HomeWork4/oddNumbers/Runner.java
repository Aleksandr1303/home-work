package oddNumbers;

public class Runner {

	public static void main(String[] args) {
		// подсчет количества нечетных чисел в массиве:

		int[] arr = new int[] { 0, 5, 2, 4, 7, 1, 3, 19 };

		int sum;
		sum = 0;

		for (int i = 0; i < arr.length; i++) {
			if (arr[i] % 2 == 1) {
				sum += 1;
			}
		}
		System.out.println("Количество нечетных чисел в массиве = " + sum + ".");

	}

}
