package randomArray;

import java.util.Random;
import java.util.Arrays;

public class Runner {

	public static void main(String[] args) {
		// массив случайных чисел, заполненных удвоенными значениями начальных:

		int[] arr = new int[15];

		Random rd = new Random();

		for (int i = 0; i < arr.length; i++) {
			arr[i] = rd.nextInt(15);
		}
		System.out.println("Массив случайных чисел размером 15 элементов:\r" + Arrays.toString(arr) + ";");

		int[] copyArr = Arrays.copyOfRange(arr, 0, arr.length * 2);

		for (int j = 0; j < copyArr.length; j++) {
			if (j > 14) {
				copyArr[j] = copyArr[j - arr.length] * 2;
			}
		}
		System.out.println(
				"Массив в два раза больше с остальными удвоенными значениями, которые идут после первых 15:\r" + Arrays.toString(copyArr) + ";");
	}

}
