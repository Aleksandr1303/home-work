package longWord;

import java.util.Scanner;
import java.util.Arrays;

public class Main {

	public static void main(String[] args) {
		// находим самое длинное слово и выводим его на экран:

		Scanner scan = new Scanner(System.in);

		String text;
		System.out.println("Введите текст из слов, разделенных пробелами:");
		text = scan.nextLine();

		int countSymbols = 0;
		String longWord = "";
		String[] words = text.split("[ ]");
		for (int i = 0; i < words.length; i++) {

			char[] sym = words[i].toCharArray();

			if (countSymbols < sym.length) {
				countSymbols = sym.length;
				longWord = words[i];
			}
		}
		System.out.println("Самое длинное слово в вашем тексте состоит из " + countSymbols + " символов и это слово - "
				+ longWord + ".");
	}

}
