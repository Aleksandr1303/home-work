package numberPi;

public class Main {

	public static void main(String[] args) {
		// выведение на экран 10 строк со значением числа Пи:

		double numberPi = 3.1415926535897;
		int counter = 2;
		int j = 1;

		for(int i =0;i<10;i++) {
			String stringPi = String.format((j++)+" - Значение числа Pi = %." + (counter++) + "f", numberPi);
			System.out.println(stringPi);
		}
	}

}
