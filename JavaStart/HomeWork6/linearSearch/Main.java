package linearSearch;

import java.util.Arrays;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// реализация линейного поиска элемента в массиве целых чисел:

		Scanner sc = new Scanner(System.in);
		int number;
		int[] array = new int[] { -7, 8, 33, 37, 77, 87, -5, 25, 13, 11, 10, 3, 131 };

		System.out.println("Введите число из массива, индекс которого хотите получить, если оно есть в маасиве:"
				+ Arrays.toString(array) + " ?");

		number = sc.nextInt();

		System.out.println("Index element: " + linearSearchElementInArray(array, number) + ".");

	}

	public static int linearSearchElementInArray(int[] arr, int number) {
		for (int i = 0; i < arr.length; i++) {
			if (arr[i] == number) {
				return i;
			}
		}
		return -1;
	}

}
