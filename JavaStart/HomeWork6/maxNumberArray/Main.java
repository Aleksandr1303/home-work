package maxNumberArray;

import java.util.Arrays;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// метод, который возвращает максимальное число из массива целых чисел:

		Scanner sc = new Scanner(System.in);
		int[] array;
		System.out.println("Введите размер массива:");
		int a = sc.nextInt();
		array = new int[a];

		for (int i = 0; i < array.length; i++) {
			System.out.println("Введите " + (i + 1) + "-ое значение массива - " + Arrays.toString(array));
			array[i] = sc.nextInt();
		}

		int max = maxNumberArray(array);
		System.out.println("Максимальное значение вашего массива " + Arrays.toString(array) + " - " + max + ".");
	}

	public static int maxNumberArray(int[] arr) {
		int max;
		max = arr[0];
		for (int i = 0; i < arr.length; i++) {
			if (arr[i] > max) {
				max = arr[i];
			}
		}

		return max;
	}

}
