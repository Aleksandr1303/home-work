package rectangleStar;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// метод, рисующий на экране прямоугольник из звездочек:

		Scanner sc = new Scanner(System.in);
		int height;
		int width;
		System.out.println("Введите высоту прямоугольника (числовое значение): ");
		height = sc.nextInt();
		System.out.println("Введите ширину прямоугольника (числовое значение): ");
		width = sc.nextInt();

		paintRectangle(height, width);

	}

	public static void paintRectangle(int vertical, int horizontal) {

		for (int i = 1; i <= vertical; i++) {
			for (int j = 1; j <= horizontal; j++) {
				if (i == vertical || i == 1 || j == 1 || j == horizontal) {
					System.out.print("*");
				} else {
					System.out.print(" ");
				}

			}
			System.out.println();
		}
	}
}