package subsequence;

import java.util.Arrays;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// программа выводит следующий член последовательности:

		Scanner sc = new Scanner(System.in);
		int[] array;
		array = new int[5];

		for (int i = 0; i < array.length; i++) {
			System.out.print("Введите " + (i + 1) + "-ое значение - ");
			array[i] = sc.nextInt();
		}
		System.out.println(array[0] + "; " + array[1] + "; " + array[2] + "; " + array[3] + "; " + array[4] + "; ");
		long result = 0;

		long nextMemberOfTheSequence = 0;
		if (arithmeticProgression(array) != 0) {
			nextMemberOfTheSequence = arithmeticProgression(array);
			System.out.println("Следующий член этой последовательности - " + nextMemberOfTheSequence);
		} else if (geometricProgression(array, result) != 0) {
			nextMemberOfTheSequence = geometricProgression(array, result);
			System.out.println("Следующий член этой последовательности - " + nextMemberOfTheSequence);
		} else if (degreeInSeries(array, result) != 0) {
			nextMemberOfTheSequence = degreeInSeries(array, result);
			System.out.println("Следующий член этой последовательности - " + nextMemberOfTheSequence);
		} else if (exponentiate(array, result) != 0) {
			nextMemberOfTheSequence = exponentiate(array, result);
			System.out.println("Следующий член этой последовательности - " + nextMemberOfTheSequence);
		} else {
			System.out.println("ERROR!");
		}
	}

	public static long arithmeticProgression(int[] arr) {

		int difArProgression = arr[1] - arr[0];

		for (int i = 1; i < arr.length; i++) {
			if (!(arr[i] == arr[i - 1] + difArProgression)) {
				return 0;
			}
		}
		return arr[arr.length - 1] + difArProgression;
	}

	public static long geometricProgression(int[] arr, long result) {
		if (arr[0] == 0) {
			return 0;
		}
		int q = arr[1] / arr[0];
		for (int i = 1; i < arr.length; i++) {
			if (!(arr[i] == arr[i - 1] * q)) {
				return 0;
			} else {
				result = arr[i] * q;
			}
		}
		return result;
	}

	public static long degreeInSeries(int[] arr, long result) {
		int step = (arr[1] - arr[0]) - 1;
		int sequenceStep = 1 + step;

		for (int i = 1; i < arr.length; i++) {
			if (!(arr[i] - arr[i - 1] == sequenceStep)) {
				return 0;
			} else {
				sequenceStep += step;
				result = arr[i] + sequenceStep;
			}
		}
		return result;
	}

	public static long exponentiate(int[] arr, long result) {
		double exp = Math.log(arr[1]) / Math.log(2);
		for (int i = 1; i < arr.length; i++) {
			if (Math.pow(i, exp) == arr[i - 1]) {
				result = (long) Math.pow(i + 2, exp);
			} else {
				result = 0;
				break;
			}
		}
		return result;
	}

}