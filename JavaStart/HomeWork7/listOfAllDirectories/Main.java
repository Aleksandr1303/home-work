package listOfAllDirectories;

import java.io.File;

public class Main {

	public static void main(String[] args) {
		// вывести на экран список всех каталогов, расположенных в каталоге, адрес
		// которого будет параметром этого метода:

		File directory = new File("MainDirectory");

		directory.mkdirs();

		System.out.println(directory);
		showDisplayListOfAllDirectories(directory);

	}

	public static void showDisplayListOfAllDirectories(File dir) {
		File lang1 = new File(dir, "JS");
		lang1.mkdirs();
		File lang2 = new File(dir, "Java");
		lang2.mkdirs();
		File lang3 = new File(dir, "Python");
		lang3.mkdirs();
		File nesDir1 = new File(lang1, "data_types");
		nesDir1.mkdirs();
		File nesDir2 = new File(lang2, "data");
		nesDir2.mkdirs();

		File[] allLang = dir.listFiles();
		for (File element : allLang) {
			if (element.isDirectory()) {
				System.out.println(element);
				File[] nestedDir = element.listFiles();
				for (File elem : nestedDir) {
					System.out.println(elem);
				}
			}
		}

	}
}
