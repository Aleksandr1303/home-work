package textEditor;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Runner {

	public static void main(String[] args) {
		// "текстовый редактор" с возможностью сохранения набранного текста в файл:
		Scanner sc = new Scanner(System.in);
		String text = "";
		System.out.println("Введите текст: ");
		text = sc.nextLine();

		File newFile = new File("text.java");

		writingTextToFile(text, newFile);
	}

	public static void writingTextToFile(String text, File newFile) {

		try (PrintWriter pw = new PrintWriter(newFile)) {
			pw.println(text);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
