package twoDimensionalArray;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;

public class Main {

	public static void main(String[] args) {
		// сохранение в текстовый файл двухмерного массива целых чисел:

		int[][] arr = new int[][] { { 5, 10, 15 }, { 10, 20, 30 }, { 77, 87, 107 }, { 13 } };

		File file_new = new File("twoDimensionalArray.txt");

		try {
			file_new.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
		}

		savingTwoDimensionalArrayToFile(arr, file_new);

	}

	public static void savingTwoDimensionalArrayToFile(int[][] array, File file1) {
		try (PrintWriter pw = new PrintWriter(file1)) {

			for (int i = 0; i < array.length; i++) {

				for (int j = 0; j < array[i].length; j++) {
					pw.print(array[i][j]);
				}
				pw.println();
			}

//			for (int[] arrayString : array) {
//				pw.println(Arrays.toString(arrayString));
//			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
