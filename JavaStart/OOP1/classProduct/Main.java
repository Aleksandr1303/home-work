package classProduct;

public class Main {

	public static void main(String[] args) {
		// пользовательский класс для описания товара:

		Product groats1 = new Product("sugar", 0.5, 1.10);

		Product groats2 = new Product("salt", 0.8, 1);

		Product groats3 = new Product();
		groats3.setName("buckwheat");
		groats3.setWeight(1);
		groats3.setPrice(4.8);

		Product groats4 = new Product();
		groats4.setName("rice");
		groats4.setWeight(0.8);
		groats4.setPrice(5);

		System.out.println(groats1.toString());
		System.out.println(groats2);
		System.out.println(groats3);
		System.out.println(groats3.getName());
		System.out.println(groats4);

		System.out.println("нет на остатке - " + groats4.getCost());
		groats4.setAmount(15);
		groats4.costProduct(15);
		System.out.println(groats4.getAmount() + " - " + groats4.getCost());
	}

}
