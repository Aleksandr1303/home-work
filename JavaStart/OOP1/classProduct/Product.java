package classProduct;

public class Product {

	private String name;
	private double weight;
	private double price;
	private double cost;
	private int amount;

	public Product(String name, double weight, double price) {
		this.name = name;
		this.weight = weight;
		this.price = price;
	}

	public Product() {
	}

	public String getName(Product this) {
		return name;
	}

	public void setName(Product this, String name) {
		this.name = name;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public double getCost() {
		return cost;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public double costProduct(int amount) {
		cost = amount * price;
		System.out.println("Стоимость за " + amount + "ед. = " + cost + " euro.");
		return cost;
	}

	public String toString() {
		return "Product [name=" + name + ", weight=" + weight + " kg, price=" + price + " euro]";
	}
}
