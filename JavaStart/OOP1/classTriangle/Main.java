package classTriangle;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// метод возвращает площадь треугольника:

		Triangle triangle1 = new Triangle();

		triangle1.setSideA(7);
		triangle1.setSideB(10);
		triangle1.setSideC(13);

		triangle1.calcAreaOfTriangle();

		System.out.println("Полупериметр = " + triangle1.getP());
		System.out.println(triangle1);

		Scanner sc = new Scanner(System.in);
		double a;
		double b;
		double c;
		System.out.println("Введите первую сторону треугольника a = ");
		a = sc.nextDouble();
		System.out.println("Введите вторую сторону треугольника b = ");
		b = sc.nextDouble();
		System.out.println("Введите третью сторону треугольника c = ");
		c = sc.nextDouble();
		Triangle triangle2 = new Triangle(a, b, c);

		triangle2.calcAreaOfTriangle();

		System.out.println("Полупериметр = " + triangle2.getP());
		System.out.println(triangle2);

	}

}
