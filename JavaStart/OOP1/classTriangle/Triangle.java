package classTriangle;

public class Triangle {
	private double sideA;
	private double sideB;
	private double sideC;
	private double p;
	private double sTriangle;

	public Triangle(double sideA, double sideB, double sideC) {
		this.sideA = sideA;
		this.sideB = sideB;
		this.sideC = sideC;
	}

	public Triangle() {
	}

	public double getSideA() {
		return sideA;
	}

	public void setSideA(double sideA) {
		this.sideA = sideA;
	}

	public double getSideB() {
		return sideB;
	}

	public void setSideB(double sideB) {
		this.sideB = sideB;
	}

	public double getSideC() {
		return sideC;
	}

	public void setSideC(double sideC) {
		this.sideC = sideC;
	}

	public double getP() {
		return p;
	}

	public double getsTriangle() {
		return sTriangle;
	}

	public double calcHalfPerimetr(Triangle this) {
		p = 0.5 * (this.sideA + sideB + sideC);
		return p;
	}

	public double calcAreaOfTriangle() {
		calcHalfPerimetr();
		sTriangle = Math.sqrt(p * (p - sideA) * (p - sideB) * (p - sideC));
		return sTriangle;
	}

	@Override
	public String toString() {
		return "Triangle [sideA=" + sideA + ", sideB=" + sideB + ", sideC=" + sideC + ", p=" + p + ", sTriangle="
				+ sTriangle + "]";
	}

}
