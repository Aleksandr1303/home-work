package animals;

public class Cat extends Animal {
	private String name;

	public Cat(String ration, String color, int weight, String name) {
		super(ration, color, weight);
		this.name = name;
	}

	public Cat() {
		super();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getVoice() {
		return "meow";
	}

	public void eat() {
		System.out.println("eat fish");
	}

	public void sleep() {
		System.out.println("sleeps during the day");
	}

	@Override
	public String toString() {
		return "Cat [name=" + name + ", ration=" + getRation() + ", color=" + getColor() + ", weight=" + getWeight()
				+ ", voice=" + getVoice() + "]";
	}

}
