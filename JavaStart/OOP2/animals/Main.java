package animals;

public class Main {

	public static void main(String[] args) {
		// применение наследования:

		Cat cat1 = new Cat("milk", "grey", 4, "Sebastian");

		System.out.println(cat1.toString());

		Dog dog1 = new Dog();
		dog1.setName("Pluto");
		dog1.setRation("dog food");
		dog1.setColor("black");
		dog1.setWeight(7);

		System.out.println(dog1.toString());

		Veterinarian doctor = new Veterinarian("Alex");

		doctor.treatment(cat1);
		doctor.treatment(dog1);

	}

}
