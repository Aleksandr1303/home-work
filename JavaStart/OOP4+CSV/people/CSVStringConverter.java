package people;

public class CSVStringConverter implements StringConverter {

	@Override
	public String toStringRepresentation(Student student) {
		return student.getName() + "," + student.getLastName() + "," + student.getGender() + "," + student.getId() + ","
				+ student.getGroupName();
	}

	@Override
	public Student fromStringRepresentation(String str) {

		String[] csv = str.split(",");
		Student studik = new Student();

		studik.setName(csv[0]);
		studik.setLastName(csv[1]);
		studik.setGender(Gender.valueOf(csv[2]));
		studik.setId(Integer.parseInt(csv[3]));
		studik.setGroupName(csv[4]);

		return studik;
	}

}
