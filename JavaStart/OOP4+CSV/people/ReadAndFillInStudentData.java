package people;

import java.util.InputMismatchException;
import java.util.Scanner;

public class ReadAndFillInStudentData {
	private String name;
	private String lastName;
	private Gender gender;
	private int id;
	private String groupName;

	Scanner scan = new Scanner(System.in);

	public String newStudentName() {
		char a = 0;
		if (name != null) {
			System.out.println("Change name: (" + name + ") ? y or n : ");

			a = scan.next().charAt(0);
			if (a == 'n') {
				scan.nextLine();
				return name;
			}
			scan.nextLine();
		}
		if (name == null || a == 'y') {
			System.out.println("Enter the name of the new student: ");
			name = scan.nextLine();
		}
		return name;
	}

	public String newStudentLastName() {
		char a = 0;
		if (lastName != null) {
			System.out.println("Change lastName: (" + lastName + ") ? y or n : ");

			a = scan.next().charAt(0);
			if (a == 'n') {
				scan.nextLine();
				return lastName;
			}
			scan.nextLine();
		}
		if (lastName == null || a == 'y') {
			System.out.println("Enter the lastName of the new student: ");
			lastName = scan.nextLine();
		}
		return lastName;
	}

	public Gender newStudentGender() {

		System.out.println("Enter the gender of the new student: ");

		while (gender == null) {
			try {
				gender = gender.valueOf(scan.nextLine());
			} catch (IllegalArgumentException e) {
				gender = null;
				System.out.println("Incorrect gender! Try again:");
			}
		}
		return gender;
	}

	public int newStudentId() {
		System.out.println("Enter the id of the new student: ");
		if (id > 0) {
			id = scan.nextInt();
		}
		while (id <= 0) {
			try {
				id = scan.nextInt();
				if (id <= 0) {
					System.out.println("negative or 0 number. Try again: ");
				}
			} catch (InputMismatchException e) {
				System.out.println("Incorrect record book number! Try again: ");
				id = 0;
				scan.next();
			}
		}
		return id;
	}

	public String newStudentGroupName() {

		System.out.println("Enter the groupName of the new student: ");
		return groupName = scan.nextLine();
	}

	public Student setNewStudent() {

		name = newStudentName();
		lastName = newStudentLastName();
		gender = newStudentGender();
		groupName = newStudentGroupName();
		id = newStudentId();

		Student studik = new Student(name, lastName, gender, id, groupName);
		return studik;
	}

	public void addNewStudent(Student studik, Group group) {
		try {
			group.addStudent(studik);
		} catch (GroupOverflowException e) {
			e.printStackTrace();
		}
	}

	@Override
	public String toString() {
		return String.format("Student [name= %-10s", name) + String.format(", lastName= %-10s", lastName)
				+ String.format(", gender= %-6s", gender) + ", id=" + id + ", groupName=" + groupName + "] - new";
	}
}
