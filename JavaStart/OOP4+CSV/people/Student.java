package people;

public class Student extends Human {
	private int id;
	private String groupName;

	public Student(String name, String lastName, Gender gender, int id, String groupName) {
		super(name, lastName, gender);
		this.id = id;
		this.groupName = groupName;
	}

	public Student() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	@Override
	public String toString() {
		return String.format("Student [name= %-10s", getName()) + String.format(", lastName= %-10s", getLastName())
				+ String.format(", gender= %-6s", getGender()) + ", id=" + id + ", groupName=" + groupName + "]";
	}

}
