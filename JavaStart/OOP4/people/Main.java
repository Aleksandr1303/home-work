package people;

import java.util.Arrays;
import java.util.Comparator;

public class Main {

	public static void main(String[] args) {

		Gender gender = Gender.male;

		System.out.println(gender.name());

		Gender gen = gender.valueOf("female");

		System.out.println(gen);

		System.out.println(gen.getClass());

		Student studik1 = new Student("Oleksandr", "Efimenko", Gender.male, 476, "Management");
		Student studik2 = new Student("Oleksandr", "Voloshin", Gender.male, 478, "Management");
		Student studik3 = new Student("Viktoriia", "Starchenko", Gender.female, 488, "Management");
		Student studik4 = new Student("Margarita", "Gerasymova", Gender.female, 455, "Management");
		Student studik5 = new Student("Bella", "Khataeva", Gender.female, 433, "Management");
		Student studik6 = new Student("Karen", "Petit", Gender.female, 425, "Management");
		Student studik7 = new Student("Martin", "Richardson", Gender.male, 407, "Management");
//		Student studik7 = null;
		Student studik8 = new Student("Zac", "Efron", Gender.male, 413, "Management");
		Student studik9 = new Student("Dylan", "Efron", Gender.male, 412, "Management");
		Student studik10 = new Student("Julya", "Roberts", Gender.female, 403, "Management");

		System.out.println(studik1.getLastName());

		Group group = new Group();
		group.setGroupName("Marketing and Management");

		System.out.println(Arrays.toString(group.getStudents()));

		try {
			group.addStudent(studik1);
			group.addStudent(studik2);
			group.addStudent(studik3);
			group.addStudent(studik4);
			group.addStudent(studik5);
			group.addStudent(studik6);
			group.addStudent(studik7);
			group.addStudent(studik8);
			group.addStudent(studik9);
			group.addStudent(studik10);
//			group.addStudent(new Student("Vova", "Salo", Gender.male, 401, "Management"));
		} catch (GroupOverflowException e) {
			e.printStackTrace();
		}
		System.out.println(Arrays.toString(group.getStudents()));
		System.out.println(group);

		try {
			System.out.println(group.searchStudentByLastName("Efron"));
		} catch (StudentNotFoundException e) {
			e.printStackTrace();
		}

		boolean b = group.removeStudentByID(407);
		System.out.println(b);
		System.out.println(group.removeStudentByID(425));
		System.out.println(group.removeStudentByID(413));
		System.out.println(group);

		System.out.println(Arrays.toString(group.getStudents()));
		group.sortStudentsByLastName();
		System.out.println(group);
		group.sortStudentsByName();
		System.out.println(group);

		ReadAndFillInStudentData newStudent = new ReadAndFillInStudentData();
		newStudent.newStudentName();
		newStudent.newStudentLastName();
		System.out.println();
		Student studik = newStudent.setNewStudent();

		System.out.println(newStudent.toString());
		System.out.println();

		newStudent.addNewStudent(studik, group);
		System.out.println(group);
		System.out.println();

		System.out.println(newStudent.getClass());

		group.sortStudentsByLastName();
		System.out.println(group);
	}

}
