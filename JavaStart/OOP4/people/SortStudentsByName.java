package people;

import java.util.Comparator;

public class SortStudentsByName implements Comparator {

	@Override
	public int compare(Object o1, Object o2) {

		Student studik1 = (Student) o1;
		Student studik2 = (Student) o2;

		if (studik1 == null) {
			return (studik2 == null) ? 0 : -1;

		} else if (studik2 == null) {
			return (studik1 == null) ? 0 : 1;
		}

		String Name1 = studik1.getName();
		String Name2 = studik2.getName();

		if (Name1.compareTo(Name2) > 0) {
			return 1;
		}
		if (Name1.compareTo(Name2) < 0) {
			return -1;
		}

		return 0;

	}

}
