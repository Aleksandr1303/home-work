package copy;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class CopyDocFile {

	public static long copyFile(File input, File output) throws IOException {

		try (InputStream is = new FileInputStream(input); OutputStream os = new FileOutputStream(output)) {

			return is.transferTo(os);

		}
	}

	public static long copyDocsFile(File folderDocsInput, File folderDocsOutput, String str) throws IOException {

		File[] files = folderDocsInput.listFiles();
		long byteDocs = 0;

		for (File docs : files) {

			if (docs.isFile() && docs.getName().endsWith(str)) { // docs.getName().indexOf(str) >= 0

				File folderDocsCopy = new File(folderDocsOutput, docs.getName());
				byteDocs += copyFile(docs, folderDocsCopy);
			}
		}
		return byteDocs;
	}
}
