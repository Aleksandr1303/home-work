package copy;

import java.io.File;
import java.io.IOException;

public class Main {

	public static void main(String[] args) {

		File filesInput = new File("C:\\FolderWithDocs");
		File filesOutput = new File("C:\\Java Start 17.08\\OOPLesson5\\folderWithCopyDocs");

		String doc = ".doc";
		String jpg = ".jpg";
		long docsByte = 0;
		long jpgsByte = 0;

		try {
			docsByte = CopyDocFile.copyDocsFile(filesInput, filesOutput, doc);
			jpgsByte = CopyDocFile.copyDocsFile(filesInput, filesOutput, jpg);
			System.out.println(CopyDocFile.copyDocsFile(filesInput, filesOutput, ".html"));
		} catch (IOException e) {
			e.printStackTrace();
		}

		System.out.println(docsByte + " - copies of documents in bytes");
		System.out.println(jpgsByte + " - copies of pictures in bytes");
	}

}
