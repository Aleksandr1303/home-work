package people;

import java.util.Arrays;
import java.util.Comparator;

public class Group {
	private String groupName;
	private final Student[] students;

	public Group(String groupName, Student[] students) {
		super();
		this.groupName = groupName;
		this.students = students;
	}

	public Group() {
		super();
		students = new Student[10];
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public Student[] getStudents() {
		return students;
	}

	public void addStudent(Student student) throws GroupOverflowException {
		for (int i = 0;; i++) {
			if (i >= students.length) {
				GroupOverflowException exc = new GroupOverflowException("Hello, you wrong!");
				throw exc;
			}
			if (students[i] == null) {
				students[i] = student;
				break;
			}
		}
	}

	public Student searchStudentByLastName(Group this, String lastName) throws StudentNotFoundException {
		Student result = null;

		for (int i = 0; i < students.length; i++) {
			if (students[i] != null) {
				if (this.students[i].getLastName() == lastName) {
					result = this.students[i];
					break;
				}
			}
		}
		if (result == null) {
			throw new StudentNotFoundException("No student with that last name!");
		}
		return result;
	}

	public boolean removeStudentByID(int id) {
		for (int i = 0; i < students.length; i++) {
			if (getStudents()[i] == null) {
				continue;
			}
			if (students[i].getId() == id) {
				this.students[i] = null;
				return true;
			}
		}

		return false;
	}

	public void sortStudents(Comparator comp) {
		if (comp.getClass().equals(SortStudentsByLastName.class)) {
			Arrays.sort(students, Comparator.nullsFirst(new SortStudentsByLastName()));
		} else if (comp.getClass().equals(SortStudentsByName.class)) {
			Arrays.sort(students, new SortStudentsByName());
		}

	}

//	public void sortStudentsByName() {
//
//		Arrays.sort(students, new SortStudentsByName());
//	}

	@Override
	public String toString() {
		String str = "Group [groupName=" + groupName + System.lineSeparator();
		int j = 1;
		for (int i = 0; i < students.length; i++) {
			if (students[i] != null) {
				str += (j++) + " - " + students[i] + System.lineSeparator();
			}
		}
		return str + "]";
	}

}
