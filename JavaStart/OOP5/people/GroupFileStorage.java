package people;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class GroupFileStorage {

	public void saveGroupToCSV(Group gr) throws IOException {

		File csv = new File(gr.getGroupName() + ".csv");
		csv.createNewFile();

		Student[] studik = gr.getStudents();

		try (OutputStream os = new FileOutputStream(csv)) {
			for (int i = 0; i < studik.length; i++) {
				if (studik[i] != null) {
					os.write((new CSVStringConverter().toStringRepresentation(studik[i]) + System.lineSeparator())
							.getBytes());
				}

			}
		}
	}

	public Group loadGroapFromCSV(File file) throws IOException {
		Group gr = new Group();
		gr.setGroupName(file.getName().substring(0, file.getName().indexOf(".")));

		long fileLength = file.length();
		byte[] arrayBytes = new byte[(int) fileLength];

		try (InputStream is = new FileInputStream(file)) {
			is.read(arrayBytes);
			String str = new String(arrayBytes);
			String[] csvStudent = str.split(System.lineSeparator());

			for (int i = 0; i < csvStudent.length; i++) {

				gr.getStudents()[i] = new CSVStringConverter().fromStringRepresentation(csvStudent[i]);
			}
		}
		return gr;
	}

	public File findFileByGroupName(String groupName, File workFolder) throws FileNotFoundException {

		if (workFolder != null && workFolder.isDirectory()) {
			File[] folder = workFolder.listFiles();
			for (File file : folder) {
				if (file.isFile() && file.getName().equals(groupName)) {
					return file;
				}
			}
			throw new FileNotFoundException("no such file!");
		} else if (workFolder == null) {
			throw new FileNotFoundException("Your directory is null!");
		}

		throw new FileNotFoundException("no such folder!");

	}
}
