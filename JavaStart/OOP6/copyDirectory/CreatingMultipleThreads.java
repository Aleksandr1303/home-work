package copyDirectory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class CreatingMultipleThreads implements Runnable {
	private File input;
	private File output;
	private Thread thr;
	private long count;

	public CreatingMultipleThreads(File input, File output) {
		super();
		this.input = input;
		this.output = output;
		thr = new Thread(this);
		thr.start();
	}

	public Thread getThr() {
		return thr;
	}

	public long getCount() {
		return count;
	}

	@Override
	public void run() {
		File createFile = new File(output, input.getName());
		count = 0;
		try (InputStream is = new FileInputStream(input); OutputStream os = new FileOutputStream(createFile)) {
			count = is.transferTo(os);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
