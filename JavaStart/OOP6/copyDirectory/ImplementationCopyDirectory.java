package copyDirectory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class ImplementationCopyDirectory {

	public static long copyFiles(File filesInput, File filesOutput) throws IOException {
		try (InputStream is = new FileInputStream(filesInput); OutputStream os = new FileOutputStream(filesOutput)) {
			return is.transferTo(os);
		}
	}

	public static long copyDirectoryFiles(File input, File output) throws IOException {
		File[] arrayFiles = input.listFiles();
		long count = 0;
		for (File file : arrayFiles) {
			if (file.isFile()) {
				File createFile = new File(output, file.getName());
				count += copyFiles(file, createFile);
			}
		}

		return count;
	}

}
