package copyDirectory;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

public class Main {

	public static void main(String[] args) {

		System.out.println(Thread.currentThread().getName() + " - start");

		File filesInput = new File("C:\\FolderWithDocs");
		File filesOutput = new File("C:\\Java Start 17.08\\OOPLesson6\\copyDocs");
		File folderOutput = new File("C:\\Java Start 17.08\\OOPLesson6\\copyingFilesInMultipleThreads");

		long size = 0;
		long tstart = System.currentTimeMillis();
		try {
			size = ImplementationCopyDirectory.copyDirectoryFiles(filesInput, filesOutput);
		} catch (IOException e) {
			e.printStackTrace();
		}
		long tend = System.currentTimeMillis();
		System.out.println(tend - tstart + " ms - реализация одним потоком! - " + size);

		long totalSize = 0;
		tstart = System.currentTimeMillis();
		try {
			totalSize = RunThreads.runMultipleThreads(filesInput, folderOutput);
		} catch (IOException e) {
			e.printStackTrace();
		}
		tend = System.currentTimeMillis();
		System.out.println(tend - tstart + " ms - реализация несколькими потоками! - " + totalSize);
		System.out.println(Thread.currentThread().getName() + " - end");
	}
}
