package copyDirectory;

import java.io.File;
import java.io.IOException;

public class RunThreads {

	public static long runMultipleThreads(File folderInput, File folderOutput) throws IOException {
		File[] arrayFilesInDirectory = folderInput.listFiles();
		CreatingMultipleThreads[] threads = new CreatingMultipleThreads[arrayFilesInDirectory.length];
		long totalCount = 0;
		for (int i = 0; i < arrayFilesInDirectory.length; i++) {
			if (arrayFilesInDirectory[i].isFile()) {
				threads[i] = new CreatingMultipleThreads(arrayFilesInDirectory[i], folderOutput);
			}
		}
		for (int j = 0; j < threads.length; j++) {
			if (threads[j] != null) {
				try {
					threads[j].getThr().join();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				totalCount += threads[j].getCount();
			}
		}
		return totalCount;
	}
}
