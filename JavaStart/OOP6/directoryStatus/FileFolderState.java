package directoryStatus;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

public class FileFolderState implements Runnable {
	private File input;
	private Thread thr;
	private File[] arrayCopyFiles;

	public FileFolderState(File input) {
		super();
		this.input = input;
		thr = new Thread(this);
	}

	public Thread getThr() {
		return thr;
	}

	public File[] getArrayCopyFiles() {
		return arrayCopyFiles;
	}

	public void setArrayCopyFiles(File[] arrayCopyFiles) {
		this.arrayCopyFiles = arrayCopyFiles;
	}

	public File[] calcFilesInDirectory() throws IOException {
		if (!input.isDirectory()) {
			IOException exc = new IOException("This is not a Directory!");
			throw exc;
		}
		File[] arrayFiles = input.listFiles();
		arrayCopyFiles = new File[arrayFiles.length];
		for (int i = 0; i < arrayCopyFiles.length; i++) {
			arrayCopyFiles[i] = arrayFiles[i];
		}
		return arrayCopyFiles;
	}

	@Override
	public void run() {
		File[] original = input.listFiles();
		File[] copy = getArrayCopyFiles();

		if (copy != null && original != null) {
			if (!(Arrays.equals(original, copy)) && original.length != copy.length) {
				if (Arrays.compare(original, copy) > 0 && copy.length > original.length) {
					copy = original;
					System.out.println("Вы удалили файл из папки!");
					System.exit(0);
				}
				if (Arrays.compare(original, copy) < 0 && copy.length < original.length) {
					copy = original;
					System.out.println("Вы добавили новый файл!");
					System.exit(0);
				}
			}
		}
	}
}
