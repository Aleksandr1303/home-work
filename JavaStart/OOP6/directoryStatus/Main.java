package directoryStatus;

import java.io.File;
import java.io.IOException;

public class Main {

	public static void main(String[] args) {
		Thread thr = Thread.currentThread();

		System.out.println(thr.getName() + " - start");

		File files = new File("C:\\FolderWithDocs");

//		1 вариант:
//		try {
//			ThreadChecksDirectoryStatus.checkStatusGivenDirectory(files);
//		} catch (IOException e) {
//			e.printStackTrace();
//		}

//		2 вариант:
		try {
			StartTrackingFileInFolder.runThreadTrackingFileInFolder(files);
		} catch (IOException e) {
			e.printStackTrace();
		}

		System.out.println(thr.getName() + " - end");
	}
}
