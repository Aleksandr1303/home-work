package directoryStatus;

import java.io.File;
import java.io.IOException;

public class StartTrackingFileInFolder {

	public static void runThreadTrackingFileInFolder(File input) throws IOException {

		FileFolderState state = new FileFolderState(input);
		File[] newCopy = state.calcFilesInDirectory();
		state.getThr().start();

		try {
			state.getThr().join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		for (;;) {
			try {
				FileFolderState state1 = new FileFolderState(input);
				state1.setArrayCopyFiles(newCopy);
				state1.getThr().start();
				state1.getThr().join();
				state1.getThr();
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
