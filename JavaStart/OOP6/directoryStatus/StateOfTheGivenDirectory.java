package directoryStatus;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;

public class StateOfTheGivenDirectory implements Runnable {

	private File input;
	private Thread thr;
	private byte[] arrayBytes;
	private byte[] copyFiles;
	private long totalBytes;
	private long copyTotalBytes;

	public StateOfTheGivenDirectory(File input) {
		super();
		this.input = input;
		thr = new Thread(this);

	}

	public Thread getThr() {
		return thr;
	}

	public byte[] getArrayBytes() {
		return arrayBytes;
	}

	public byte[] getCopyFiles() {
		return copyFiles;
	}

	public void setCopyFiles(byte[] copyFiles) {
		this.copyFiles = copyFiles;
	}

	public long getTotalBytes() {
		return totalBytes;
	}

	public long getCopyTotalBytes() {
		return copyTotalBytes;
	}

	public void setCopyTotalBytes(long copyTotalBytes) {
		this.copyTotalBytes = copyTotalBytes;
	}

	public byte[] calcDirectorySize() throws IOException {

		if (!input.isDirectory()) {
			IOException exc = new IOException("This is not a Directory!");
			throw exc;
		}
		totalBytes = 0;
		int k = 0;

		File[] arrayFiles = input.listFiles();
		arrayBytes = new byte[(int) input.length()];

		for (int i = 0; i < arrayFiles.length; i++) {
			if (arrayFiles[i].isFile()) {
				try (InputStream is = new FileInputStream(arrayFiles[i])) {
					byte[] arrayBytesFile = new byte[(int) arrayFiles[i].length()];
					totalBytes += is.read(arrayBytesFile);
					for (int j = 0; j < arrayBytesFile.length; j++) {
						arrayBytes[k] = arrayBytesFile[j];
						if ((arrayBytes.length - 1) <= k) {
							break;
						}
						k++;
					}
				}
			}
		}
		return arrayBytes;
	}

	public byte[] copyDirectoryData() throws IOException {
		copyFiles = Arrays.copyOf(calcDirectorySize(), (int) input.length());
		copyTotalBytes = totalBytes;
		return copyFiles;
	}

	@Override
	public void run() {
		byte[] original = null;
		try {
			original = calcDirectorySize();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		byte[] copy = copyFiles;

		if (copy != null && original != null) {
			if (!(Arrays.equals(original, copy)) || copyTotalBytes != totalBytes) {
				if (Arrays.compare(original, copy) > 0 || copyTotalBytes > totalBytes) {
					System.out.println("Вы удалили файл из папки!");
//					copy = original;
					System.exit(0);
				}
				if (Arrays.compare(original, copy) < 0 || copyTotalBytes < totalBytes) {
					System.out.println("Вы добавили новый файл!");
//					copy = original;
					System.exit(0);
				}
			}
		}
	}
}
