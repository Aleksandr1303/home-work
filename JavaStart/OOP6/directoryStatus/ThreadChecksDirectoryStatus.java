package directoryStatus;

import java.io.File;
import java.io.IOException;

public class ThreadChecksDirectoryStatus {

	public static void checkStatusGivenDirectory(File input) throws IOException {

		StateOfTheGivenDirectory state = new StateOfTheGivenDirectory(input);
		byte[] newCopy = state.copyDirectoryData();
		long copyBytes = state.getCopyTotalBytes();
		state.getThr().start();

		try {
			state.getThr().join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		for (;;) {
			try {
				StateOfTheGivenDirectory state1 = new StateOfTheGivenDirectory(input);
				state1.setCopyFiles(newCopy);
				state1.setCopyTotalBytes(copyBytes);
				state1.getThr().start();
				state1.getThr().join();
				state1.getThr();
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
