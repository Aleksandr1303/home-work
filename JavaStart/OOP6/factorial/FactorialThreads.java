package factorial;

import java.math.BigInteger;

public class FactorialThreads implements Runnable {

	public BigInteger calcFactorialOfNumber(long number) {
		BigInteger fact = BigInteger.ONE;
		for (int i = 1; i <= number; i++) {
			fact = fact.multiply(BigInteger.valueOf(i));
		}
		return fact;
	}

	@Override
	public void run() {

		Thread thr = Thread.currentThread();

		System.out.println(thr.getId() + "(ID потока) - " + thr.getName() + " - factorial " + thr.getId() + "! = "
				+ this.calcFactorialOfNumber(thr.getId()));

	}

}
