package factorial;

public class Main {

	public static void main(String[] args) {

		Thread[] ArrayThreads = new Thread[100];

		for (int i = 0; i < ArrayThreads.length; i++) {
			ArrayThreads[i] = new Thread(new FactorialThreads());
			ArrayThreads[i].start();
		}
	}
}
