package sumElements;

public class ArrayOfIntegers {

	public static long calcSumOfRandomNumbersInArray(int[] arrayOfNumbers) {

		long sum = 0;
		for (int i = 0; i < arrayOfNumbers.length; i++) {

			sum = sum + arrayOfNumbers[i];

		}
//		System.out.println("Сумма элементов массива - " + sum);
		return sum;
	}

}
