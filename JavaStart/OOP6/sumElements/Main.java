package sumElements;

public class Main {

	public static void main(String[] args) {

		Thread threadMain = Thread.currentThread();
		System.out.println(threadMain.getName() + " - start");

		int[] arrayIntegers = new int[200000000];
		for (int i = 0; i < arrayIntegers.length; i++) {
			arrayIntegers[i] = (int) Math.floor(Math.random() * 255);
		}

		long tstart = System.currentTimeMillis();
		long sumInOneStream = ArrayOfIntegers.calcSumOfRandomNumbersInArray(arrayIntegers);
		long tend = System.currentTimeMillis();
		System.out.println(tend - tstart + " ms - " + "Сумма, полученная в одном потоке - " + sumInOneStream);

		tstart = System.currentTimeMillis();

		StreamForCountingTheSum sum1 = new StreamForCountingTheSum(arrayIntegers, 0, 50000000);
		StreamForCountingTheSum sum2 = new StreamForCountingTheSum(arrayIntegers, 50000000, 100000000);
		StreamForCountingTheSum sum3 = new StreamForCountingTheSum(arrayIntegers, 100000000, 150000000);
		StreamForCountingTheSum sum4 = new StreamForCountingTheSum(arrayIntegers, 150000000, 200000000);

		Thread[] thr = new Thread[] { new Thread(sum1), new Thread(sum2), new Thread(sum3), new Thread(sum4) };

		for (int i = 0; i < thr.length; i++) {
			thr[i].start();
		}
		for (int i = 0; i < thr.length; i++) {
			try {
				thr[i].join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		long totalSum = sum1.getPartSum() + sum2.getPartSum() + sum3.getPartSum() + sum4.getPartSum();
		tend = System.currentTimeMillis();
		System.out.println(tend - tstart + " ms - " + "Сумма, полученная в четырех параллельных потоках - " + totalSum);

		System.out.println(threadMain.getName() + " - end");
	}
}
