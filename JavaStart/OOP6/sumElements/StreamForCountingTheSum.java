package sumElements;

public class StreamForCountingTheSum implements Runnable {

	private int start;
	private int end;
	private int[] arrayOfNumbers;
	private long partSum = 0;

	public StreamForCountingTheSum(int[] arrayOfNumbers, int start, int end) {
		super();
		this.start = start;
		this.end = end;
		this.arrayOfNumbers = arrayOfNumbers;
	}

	public long getPartSum() {
		return partSum;
	}

	@Override
	public void run() {
		for (int i = start; i < end; i++) {
			partSum = partSum + arrayOfNumbers[i];
		}
	}
}
