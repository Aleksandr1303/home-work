package shipsWithCargo;

public class Control {

	private int dock1 = 0;
	private int dock2 = 0;
	private Ship ship1;
	private Ship ship2;
	private Ship ship3;
	private int count = 1;
	private boolean or = false;
	private boolean stop = false;

	public void setDock1(int dock1) {
		this.dock1 = dock1;
	}

	public void setDock2(int dock2) {
		this.dock2 = dock2;
	}

	public void setControl(Ship ship1, Ship ship2, Ship ship3) {
		this.ship1 = ship1;
		this.ship2 = ship2;
		this.ship3 = ship3;
	}

	private Ship getShip() {
		if (count == 1) {
			return ship1;
		}
		if (count == 2) {
			return ship2;
		}
		if (count == 3) {
			return ship3;
		}
		return null;
	}

	public synchronized void setDocks(Ship ship) {
		if (getShip() != null && ship != null) {
			for (; or == true || getShip() != ship;) {
				try {
					System.out.println("корабль №" + Thread.currentThread().getId() + " Стал на паузу!");
					wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			if (ship.getBox() > 0) {
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				this.dock1 = ship.getBox() - (ship.getBox() - 1);
				ship.setBox(ship.getBox() - dock1);
				System.out.println("Выгружаем груз в dock1 - " + dock1 + " ящик");
			}
			if (!or && dock1 == 1 && ship.getBox() > 0) {
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				this.dock2 = ship.getBox() - (ship.getBox() - 1);
				ship.setBox(ship.getBox() - dock2);
				System.out.println("Выгружаем груз в dock2 - " + dock2 + " ящик");
			}
			System.out.println("корабль № " + Thread.currentThread().getId() + " остаток ящиков - " + ship.getBox());
			or = true;
		}
		count += 1;
		if (count > 3) {
			count = 1;
		}
		notifyAll();
	}

	public synchronized int getDocks() {
		for (; or == false;) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		or = false;
		notifyAll();
		if (ship1.getBox() == 0 && ship2.getBox() == 0 && ship3.getBox() == 0) {
			setStop(true);
		}
		if (dock1 != 0 || dock2 != 0) {
			return dock1 + dock2;
		}
		return 0;
	}

	public boolean isStop() {
		return stop;
	}

	public void setStop(boolean stop) {
		this.stop = stop;
	}
}
