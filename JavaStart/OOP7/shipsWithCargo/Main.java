package shipsWithCargo;

public class Main {

	public static void main(String[] args) {

		Control control = new Control();

		Ship ship1 = new Ship(control, 10);
		Ship ship2 = new Ship(control, 10);
		Ship ship3 = new Ship(control, 10);

		control.setControl(ship1, ship2, ship3);

		Thread[] thr = new Thread[] { new Thread(ship1), new Thread(ship2), new Thread(ship3) };

		for (Thread ship : thr) {
			ship.start();
		}

		Port port = new Port(control);
		System.out.println(port.getDock1());
		System.out.println(port.getDock2());
		System.exit(0);
	}
}
