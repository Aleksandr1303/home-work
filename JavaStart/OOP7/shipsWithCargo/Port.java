package shipsWithCargo;

public class Port implements Runnable {

	private Control control;
	private int dock1;
	private int dock2;

	public Port(Control control) {
		super();
		this.control = control;
		Thread thr = new Thread(this);
		thr.start();
		try {
			thr.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public int getDock1() {
		return dock1;
	}

	public int getDock2() {
		return dock2;
	}

	@Override
	public void run() {
		System.out.println("№ Порта, принимающего груз - " + Thread.currentThread().getId());
		int i = 0;
		for (; !control.isStop();) {
			int port = control.getDocks();
			if (port > 1) {
				dock1 += port - 1;
				dock2 += port - 1;
			}
			if (port == 1 && dock1 == dock2) {
				dock1 += port;
			} else if (port == 1 && dock1 > dock2) {
				dock2 += port;
			}
			control.setDock1(0);
			control.setDock2(0);
			System.out.println(
					"№ операции - " + (++i) + ": (first dock - " + dock1 + "box) - (second dock - " + dock2 + "box).");
			System.out.println();
		}
	}
}
