package shipsWithCargo;

public class Ship implements Runnable {

	private Control control;
	private int box;

	public Ship(Control control, int box) {
		super();
		this.control = control;
		this.box = box;
	}

	public int getBox() {
		return box;
	}

	public void setBox(int box) {
		this.box = box;
	}

	@Override
	public void run() {
		System.out.println("№ Разгружаемого корабля - " + Thread.currentThread().getId());

		for (; !control.isStop();) {
			control.setDocks(this);
		}
	}
}
